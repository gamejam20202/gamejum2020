#pragma once
#include "../../engine/engine.h"
#include <math.h>
#include "obstacle.h"

class Ufo :public Obstacle {
public:
	Ufo(const Image::DivInfo& div_info, Transform& transform, const int around);
	virtual ~Ufo();

	// 更新
	virtual void update(const float delta_time);

	// 描画
	virtual void render();

	// 他のCollider付きのGameObjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);

	float time_count = 0;
	int around = 0;
	float move_x;
	float sin_as_count;
};