#include "right_ui.h"
#include "player.h"

RightUi::RightUi(Player* player, Transform& transform) : GameObject(transform), player_(player) {
	setAffectCamera2DRate(0);
	setLayer(4);

	guide_[EYE_OPEN][MOUTH_OPEN].loadImage("graphics/op/op_3.png");
	guide_[EYE_OPEN][MOUTH_CLOSE].loadImage("graphics/op/op_4.png");
	guide_[EYE_CLOSE][MOUTH_OPEN].loadImage("graphics/op/op_2.png");
	guide_[EYE_CLOSE][MOUTH_CLOSE].loadImage("graphics/op/op_1.png");
}

RightUi::~RightUi() {
	if (speech_bubble_) {
		delete speech_bubble_;
	}
	if (message_) {
		delete message_;
	}
}

bool RightUi::seqEyeOpen(const float delta_time) {

	// 目を開く
	if (seq_eye_.isStart()) {
		eye_ = EYE_OPEN;
	}

	// 2秒間は瞬きしない
	if (seq_eye_.getProgressTime() < 2.0f) return true;

	// 2秒後は 1 / 60 の確率で瞬きする
	if (rand() % 120 == 0) {
		seq_eye_.change(&RightUi::seqEyeClose);
	}

	return true;
}

bool RightUi::seqEyeClose(const float delta_time) {

	// 目を閉じる
	if (seq_eye_.isStart()) {
		eye_ = EYE_CLOSE;
	}

	// 0.1秒経ったら目を開ける（秒数調整要）
	if (0.1f < seq_eye_.getProgressTime()) {
		seq_eye_.change(&RightUi::seqEyeOpen);
	}

	return true;
}

bool RightUi::seqMouthOpen(const float delta_time) {
	// 口を開ける
	if (seq_mouth_.isStart()) {
		mouth_ = MOUTH_OPEN;
	}

	// 0.2秒経ったら口を開ける（秒数調整要）
	if (0.15f < seq_mouth_.getProgressTime()) {
		seq_mouth_.change(&RightUi::seqMouthClose);
	}

	return true;
}

bool RightUi::seqMouthClose(const float delta_time) {
	// 口を閉じる
	if (seq_mouth_.isStart()) {
		mouth_ = MOUTH_CLOSE;
	}

	// 0.2秒経ったら口を開ける（秒数調整要）
	if (0.15f < seq_mouth_.getProgressTime()) {
		seq_mouth_.change(&RightUi::seqMouthOpen);
	}

	return true;
}

void RightUi::update(const float delta_time) {

	// 乱数で瞬き
	seq_eye_.update(delta_time);

	// メッセージ表示がある時に口パクする
	if (message_) {
		seq_mouth_.update(delta_time);
	}
	// メッセージ表示がない時で口が閉じてない時は口を閉じる
	else if (mouth_ != MOUTH_CLOSE) {
		mouth_ = MOUTH_CLOSE;
	}
}

void RightUi::render() {
	SetDrawBlendMode(blend_mode_, blend_parameter_);

	bg_.render(transform_);

	// 座標指定しなおす↓
	guide_[eye_][mouth_].render(transform_);

	// メッセージ関係
	if (speech_bubble_) {
		// 座標指定しなおす↓
		speech_bubble_->render(transform_);
	}

	if (message_) {
		// 座標指定しなおす↓
		message_->render(x_, y_, font_size_, color_, interval_);
	}
}
