#pragma once
#include "../../engine/engine.h"


class Item : public GameObject {
public:
	Item(const char* filename, Transform& transform);
	virtual ~Item();

	Image image_;

	//更新
	virtual void update(const float delta_time);

	//描画
	virtual void render();

	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other,const int direction);

};