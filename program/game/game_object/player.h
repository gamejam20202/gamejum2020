#pragma once
#include "../../engine/engine.h"


class Player : public GameObject {
public:
	Player(Transform& transform = Transform());
	~Player();

	Image image_ = Image("graphics/icon_roke_anime.png", 2, 2, 1, 48, 94);
	Image visibility_ = Image("graphics/visibility.png");

	Normalize x_;
	Normalize y_;

	// 視界系
	const float VISIBILLITY_VAL_SUB = 0.0015f;		// 毎フレームの減少量
	const float VISIBILLITY_VAL_MIN = 1.46f;		// 最小値
	const float VISIBILLITY_VAL_MAX = 7.4f;			// 最大値
	const float VISIBILLITY_VAL_RECOVERY = 1.0f;	// 補給アイテム1つでの増加量
	const float VISIBILLITY_RECOVERY_FRAME = 0.1f;	// 補給アイテムを取った時の毎フレームの増加量
	float visibility_recovery_count_ = 0;			// 補給アイテムを取った時の総増加量
	bool is_recovery_visibility_ = false;			// 補給アイテムを獲得したか
	int supply_count_ = 0;							// 補給アイテム獲得数（連続して取った時に個別で回復する用）
	float visibility_val_ = VISIBILLITY_VAL_MAX;	// 視界

	bool special_ = true;		// 視界全快

	bool is_hit_obstacle_ = false;			// 障害物に当たったか
	const float INVINCIBLE_TIME = 1.0f;		// 障害物に当たった時の無敵時間

	const int MAX_HP = 3;		// 最大耐久力、残機
	int hp_ = MAX_HP;			// 現在の耐久力、残機

	int flash_count_ = 0;			// 障害物に当たった時の点滅用
	bool is_render_player_ = true;	// 自機描画フラグ

	const int COLLECTION_MAX = 5;
	int collection_ = 0;

	t2k::Sequence<Player*> seq_ = t2k::Sequence<Player*>(this, &Player::seqIdle);

	bool seqIdle(const float delta_time);
	bool seqHitObstacle(const float delta_time);

	void update(const float delta_time) override;

	void render() override;

	void hit(GameObject& other, const int direction) override;
};
