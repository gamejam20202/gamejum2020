#pragma once
#include "item.h"
class CollectionItem : public Item {
	CollectionItem(const char* filename, Transform& transform);
	virtual ~CollectionItem();
	//更新
	virtual void update(const float delta_time);
	//描画
	virtual void render();
	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};

class CollectionItem1 : public Item {

	CollectionItem1(const char* filename, Transform& transform);
	virtual ~CollectionItem1();
	//更新
	virtual void update(const float delta_time);
	//描画
	virtual void render();
	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};

class CollectionItem2 : public Item {

	CollectionItem2(const char* filename, Transform& transform);
	virtual ~CollectionItem2();
	//更新
	virtual void update(const float delta_time);
	//描画
	virtual void render();
	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};

class CollectionItem3 : public Item {
	CollectionItem3(const char* filename, Transform& transform);
	virtual ~CollectionItem3();
	//更新
	virtual void update(const float delta_time);
	//描画
	virtual void render();
	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};

class CollectionItem4 : public Item {
	CollectionItem4(const char* filename, Transform& transform);
	virtual ~CollectionItem4();
	//更新
	virtual void update(const float delta_time);
	//描画
	virtual void render();
	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};

class CollectionItem5 : public Item {
	CollectionItem5(const char* filename, Transform& transform);
	virtual ~CollectionItem5();
	//更新
	virtual void update(const float delta_time);
	//描画
	virtual void render();
	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};