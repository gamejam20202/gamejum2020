#pragma once
#include "item.h"


class Supply : public Item {
public:
	Supply(const char* filename, Transform& transform);
	virtual ~Supply();

	//更新
	virtual void update(const float delta_time);

	//描画
	virtual void render();

	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);

};