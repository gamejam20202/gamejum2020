#include "armor.h"


Armor::Armor(const char* filename, Transform& transform) :Item(filename, transform) {
}

Armor::~Armor() {
}

//更新
void Armor::update(const float delta_time) {
	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
}

//描画
void Armor::render() {
	image_.render(transform_);
}

//他のCollider付きのGameobjectに当たった時の処理
void Armor::hit(GameObject& other, const int direction) {

}
