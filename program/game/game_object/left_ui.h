#pragma once
#include "../../engine/engine.h"


class Player;

class LeftUi : public GameObject {
public:
	LeftUi(Player* player, Transform& transform);
	~LeftUi();

	Image bg_ = Image("graphics/bg/inu.png");
	Image player_hp_[2];


	int* zanki_ = nullptr;

	Player* player_;

	void update(const float delta_time) override;

	void render() override;
};
