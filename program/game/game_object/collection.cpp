#include "collection.h"
#include "item.h"
#include"../../engine/engine.h"
CollectionItem::CollectionItem(const char* filename, Transform& transform) : Item(filename, transform) {

}
CollectionItem::~CollectionItem() {
}
//更新
void CollectionItem::update(const float delta_time) {
}
//描画
void CollectionItem::render() {
	image_.render(transform_);
}
//他のCollider付きのGameobjectに当たった時の処理
void CollectionItem::hit(GameObject& other, const int direction) {
}

CollectionItem1::CollectionItem1(const char* filename, Transform& transform) : Item(filename, transform) {

}
CollectionItem1::~CollectionItem1() {
}
//更新
void CollectionItem1::update(const float delta_time) {
}
//描画
void CollectionItem1::render() {
	image_.render(transform_);
}
//他のCollider付きのGameobjectに当たった時の処理
void CollectionItem1::hit(GameObject& other, const int direction) {
}

CollectionItem2::CollectionItem2(const char* filename, Transform& transform) : Item(filename, transform) {

}
CollectionItem2::~CollectionItem2() {
}
//更新
void CollectionItem2::update(const float delta_time) {
}
//描画
void CollectionItem2::render() {
	image_.render(transform_);
}
//他のCollider付きのGameobjectに当たった時の処理
void CollectionItem2::hit(GameObject& other, const int direction) {
}

CollectionItem3::CollectionItem3(const char* filename, Transform& transform) : Item(filename, transform) {

}
CollectionItem3::~CollectionItem3() {
}
//更新
void CollectionItem3::update(const float delta_time) {
}
//描画
void CollectionItem3::render() {
	image_.render(transform_);
}
//他のCollider付きのGameobjectに当たった時の処理
void CollectionItem3::hit(GameObject& other, const int direction) {
}

CollectionItem4::CollectionItem4(const char* filename, Transform& transform) : Item(filename, transform) {

}
CollectionItem4::~CollectionItem4() {
}
//更新
void CollectionItem4::update(const float delta_time) {
}
//描画
void CollectionItem4::render() {
	image_.render(transform_);
}
//他のCollider付きのGameobjectに当たった時の処理
void CollectionItem4::hit(GameObject& other, const int direction) {
}

CollectionItem5::CollectionItem5(const char* filename, Transform& transform) : Item(filename, transform) {

}
CollectionItem5::~CollectionItem5() {
}
//更新
void CollectionItem5::update(const float delta_time) {
}
//描画
void CollectionItem5::render() {
	image_.render(transform_);
}
//他のCollider付きのGameobjectに当たった時の処理
void CollectionItem5::hit(GameObject& other, const int direction) {
}
