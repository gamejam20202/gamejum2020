#include "supply.h"

Supply::Supply(const char* filename, Transform& transform) :Item(filename, transform) {
}

Supply::~Supply() {
}

//更新
void Supply::update(const float delta_time) {
	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
}

//描画
void Supply::render() {
	image_.render(transform_);
}

//他のCollider付きのGameobjectに当たった時の処理
void Supply::hit(GameObject& other, const int direction) {

}
