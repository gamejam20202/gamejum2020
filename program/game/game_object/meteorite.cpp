#include "meteorite.h"


Meteorite::Meteorite(const char* file_name, Transform& transform) : Obstacle(file_name, transform) {

}

Meteorite::~Meteorite() {
}

// 更新
void Meteorite::update(const float delta_time) {
	transform_.position_.y += 1;

	//画面外に出たら生存フラグを折る
	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
}

// 描画
void Meteorite::render() {
	image_.render(transform_);
}

// 他のCollider付きのGameObjectに当たった時の処理
void Meteorite::hit(GameObject& other, const int direction) {
	
}