#include "item.h"

Item::Item(const char* filename, Transform& transform) :GameObject(transform), image_(Image(filename)) {
	collider_ = new Collider(image_.getWidth(), image_.getHeight());
	setLayer(2);
}

Item::~Item() {
	delete collider_;
}

//更新
void Item::update(const float delta_time) {

}

//描画
void Item::render() {
	image_.render(transform_);
}

//他のCollider付きのGameobjectに当たった時の処理
void Item::hit(GameObject& other, const int direction) {

}
