#pragma once
#include "../../engine/engine.h"


class Obstacle : public GameObject {
public:
	Obstacle(const Image::DivInfo& div_info, Transform& transform);
	Obstacle(const char* file_name, Transform& transform);
	virtual ~Obstacle();

	Image image_;

	// 更新
	virtual void update(const float delta_time);

	// 描画
	virtual void render();

	// 他のCollider付きのGameObjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);
};
