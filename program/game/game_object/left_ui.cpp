#include "left_ui.h"
#include "player.h"


LeftUi::LeftUi(Player* player, Transform& transform) : GameObject(transform), player_(player) {
	setAffectCamera2DRate(0);
	setLayer(4);

	player_hp_[0].loadImage("graphics/ui/icon_zanki_nasi.png");
	player_hp_[1].loadImage("graphics/ui/icon_zanki_ari.png");

	zanki_ = new int[player_->MAX_HP];

	for (int i = 0; i < player_->MAX_HP; ++i) {
		zanki_[i] = 1;
	}
}

LeftUi::~LeftUi() {
	delete[] zanki_;
}

void LeftUi::update(const float delta_time) {

	for (int i = 0; i < player_->MAX_HP; ++i) {
		if (i < player_->hp_) {
			zanki_[i] = 1;
		}
		else {
			zanki_[i] = 0;
		}
	}

}

void LeftUi::render() {
	SetDrawBlendMode(blend_mode_, blend_parameter_);
	float y = transform_.position_.y - 200;

	bg_.render(transform_.position_.x, y, 1, 0);

	// 座標は調整する↓
	for (int i = 0; i < player_->MAX_HP; ++i) {
		player_hp_[zanki_[i]].render(200 - i * 50, 650, 1, 0);
	}
	
}
