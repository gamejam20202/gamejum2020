#pragma once
#include "obstacle.h"

class Meteorite : public Obstacle {
public:
	Meteorite(const char* file_name, Transform& transform);
	~Meteorite();

	// 更新
	void update(const float delta_time) override;

	// 描画
	void render() override;

	// 他のCollider付きのGameObjectに当たった時の処理
	void hit(GameObject& other, const int direction) override;
};