#include "Starfish.h"
#include "player.h"

Starfish::Starfish(const char* file_name, Transform& transform, Player* player, GameManager* gamemanager) : Obstacle(file_name, transform) {
	//collider_ = new Collider(image_.getWidth(), image_.getHeight());
	this->player = player;
	this->gamemanager = gamemanager;
}

Starfish::~Starfish() {

}

bool Starfish::pos(const float deltatime) {
	camera_pos = gamemanager->getNowScene()->camera_.transform_.position_;
	//if (transform_.position_.y <= camera_pos.y - 320) {
	//	transform_.position_.y += 1;
	//}
	//else {
	//	seq_.change(&Starfish::idle);
	//}

	if (camera_pos.y - 320 < transform_.position_.y) {
		seq_.change(&Starfish::idle);
	}
	
	return true;
}

bool Starfish::idle(const float deltatime) {
	transform_.position_.y -= 1;
	
	b_count_ += 1;

	int x_r = -3 + (rand() % 6);
	int y_r = -3 + (rand() % 6);

	if ((b_count_ / 4) % 2 == 0) {
		transform_.position_ += t2k::Vector3(x_r, y_r, 0);
	}

	if (seq_.getProgressTime() >= 2) {
		seq_.change(&Starfish::rush);
	}
	return true;
}

bool Starfish::rush(const float deltatime) {
	if (seq_.isStart()) {
		PL_pos = player->transform_.position_;
		PL_pos.y -= 100;
		move = PL_pos - transform_.position_;
		move.normalize();
	}
	transform_.position_ += move * RUSH_SPEED;
	transform_.rotation_.z += 5;
	return true;
}

// 更新
void Starfish::update(const float delta_time) {
	seq_.update(delta_time);
	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
}

// 描画
void Starfish::render() {
	image_.render(transform_);
}

// 他のCollider付きのGameObjectに当たった時の処理
void Starfish::hit(GameObject& other, const int direction) {

}