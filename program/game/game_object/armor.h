#pragma once
#include "item.h"


class Armor : public Item {
public:
	Armor(const char* filename, Transform& transform);
	~Armor();

	//更新
	virtual void update(const float delta_time);

	//描画
	virtual void render();

	//他のCollider付きのGameobjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction);

};
