#pragma once
#include "obstacle.h"

class Player;

class Starfish : public Obstacle{
public:
	Starfish(const char* file_name, Transform& transform, Player* player, GameManager* gamemanager);
	 ~Starfish();

	 t2k::Sequence<Starfish*> seq_ = t2k::Sequence<Starfish*>(this, &Starfish::pos);

	 bool pos(const float deltatime);
	 bool idle(const float deltatime);
	 bool rush(const float deltatime);

	// 更新
	 void update(const float delta_time);

	// 描画
	 void render();

	// 他のCollider付きのGameObjectに当たった時の処理
	 void hit(GameObject& other, const int direction);

	 
	 Player* player = nullptr;
	 t2k::Vector3 PL_pos;
	 GameManager* gamemanager;
	 t2k::Vector3 camera_pos;

	 t2k::Vector3 move = { 0,0,0 };
	 const float RUSH_SPEED = 2.5f;
	 const float ROTATE_SPEED = 5.0f;

	 int b_count_ = 0;
};
