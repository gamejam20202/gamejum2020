#pragma once
#include "../../engine/engine.h"


class Player;

class RightUi : public GameObject {
public:
	RightUi(Player* player, Transform& transform);
	~RightUi();

	// 案内人のアニメーション用（目）
	enum Eye {
		EYE_OPEN,
		EYE_CLOSE,
		EYE_MAX,
	};

	// 案内人のアニメーション用（口）
	enum Mouth {
		MOUTH_OPEN,
		MOUTH_CLOSE,
		MOUTH_MAX,
	};

	Image bg_ = Image("graphics/bg/op_haikei.png");
	Image star_ = Image("graphics/item/xxx.png");
	Player* player_;

	// 案内人関係
	t2k::Sequence<RightUi*> seq_eye_ = t2k::Sequence<RightUi*>(this, &RightUi::seqEyeOpen);
	t2k::Sequence<RightUi*> seq_mouth_ = t2k::Sequence<RightUi*>(this, &RightUi::seqMouthClose);

	Eye eye_ = EYE_OPEN;					// 目のインデックス
	Mouth mouth_ = MOUTH_CLOSE;				// 口の要素数
	Image guide_[EYE_MAX][MOUTH_MAX];		// 案内人[ 目 ][ 口 ]

	// 案内人が話すメッセージ関係
	Image* speech_bubble_ = nullptr;	// 吹き出し？
	String* message_ = nullptr;			// メッセージ本体（必要な時にnewする）

	// Stringの描画プロパティ
	int x_ = 820;
	int y_ = 60;
	int font_size_ = 20;
	int color_ = 0xFF000000;
	int interval_ = 5;


	bool seqEyeOpen(const float delta_time);
	bool seqEyeClose(const float delta_time);
	bool seqMouthOpen(const float delta_time);
	bool seqMouthClose(const float delta_time);

	void update(const float delta_time);

	void render() override;
};
