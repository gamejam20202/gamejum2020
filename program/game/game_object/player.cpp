#include "player.h"
#include "obstacle.h"
#include "meteorite.h"
#include "Starfish.h"
#include "ufo.h"
#include "supply.h"
#include "armor.h"
#include "collection.h"
#include "../scene/play.h"
#include "../scene/result.h"


Player::Player(Transform& transform) : GameObject(transform) {
	collider_ = new Collider(image_.getWidth(), image_.getHeight());
	setLayer(3);
}

Player::~Player() {
	delete collider_;
}

bool Player::seqIdle(const float delta_time) {

	if (seq_.isStart()) {
		is_render_player_ = true;
	}

	return true;
}

bool Player::seqHitObstacle(const float delta_time) {

	flash_count_ += 1;

	// 点滅
	if ((flash_count_ / 7) % 2 == 0) {
		is_render_player_ = false;
	}
	else {
		is_render_player_ = true;
	}

	// 一定時間無敵
	if (INVINCIBLE_TIME < seq_.getProgressTime()) {
		seq_.change(&Player::seqIdle);
		is_hit_obstacle_ = false;
	}

	return true;
}

void Player::update(const float delta_time) {

	seq_.update(delta_time);

	image_.update(delta_time);

	// 案A　キーの入力割合で慣性をつけたような感じにする

	bool x_n = t2k::Input::isKeyDown(t2k::Input::KEYBORD_LEFT);
	bool x_p = t2k::Input::isKeyDown(t2k::Input::KEYBORD_RIGHT);
	bool y_n = t2k::Input::isKeyDown(t2k::Input::KEYBORD_UP);
	bool y_p = t2k::Input::isKeyDown(t2k::Input::KEYBORD_DOWN);
	bool spe = t2k::Input::isKeyDown(t2k::Input::KEYBORD_SPACE);

	float x = x_.getValue(x_n, x_p);
	float y = y_.getValue(y_n, y_p);

	if (x_n || x_p || y_n || y_p) {
		image_.setSwitchTime(0.1f);
	}
	else {
		image_.setSwitchTime(0.2f);
	}

	x *= 0.7f;

	if (0 < y) {
		y *= 0.2f;
	}
	
	float s = sqrt(x * x + y * y) * 5.0f;

	t2k::Vector3 v = t2k::Vector3(x * 0.5f, y, 0);

	v.normalize();

	transform_.position_ += (v * s);

	if (special_) {
		if (spe) {
			visibility_val_ = VISIBILLITY_VAL_MAX;
			special_ = false;
		}
	}

	// 案B　座標をそのまま変更する

	/*

	t2k::Vector3 move;

	float speed = 0;

	if (t2k::Input::isKeyDown(t2k::Input::KEYBORD_LEFT)) {
		move.x -= 0.5f;
		speed = 2.5f;
	}

	if (t2k::Input::isKeyDown(t2k::Input::KEYBORD_RIGHT)) {
		move.x += 0.5f;
		speed = 2.5f;
	}

	if (t2k::Input::isKeyDown(t2k::Input::KEYBORD_UP)) {
		move.y -= 1;
		speed = 5;
	}

	if (t2k::Input::isKeyDown(t2k::Input::KEYBORD_DOWN)) {
		move.y += 1;
		speed = 5;
	}

	move.normalize();

	transform_.position_ += (move * speed);

	*/

	// 座標補正
	Scene* scene = GameManager::getInstance()->getNowScene();
	const int S_H_F = scene->camera_.getScreenHeight() >> 1;
	const int S_W_F = scene->camera_.getScreenWidth() >> 1;
	const int I_H_F = image_.getHeight() >> 1;
	const int I_W_F = image_.getWidth() >> 1;

	if (scene->camera_.transform_.position_.y + S_H_F < transform_.position_.y + I_H_F) {
		transform_.position_.y = scene->camera_.transform_.position_.y + S_H_F - I_H_F;
	}
	if (transform_.position_.y - I_H_F < scene->camera_.transform_.position_.y - S_H_F) {
		transform_.position_.y = scene->camera_.transform_.position_.y - S_H_F + I_H_F;
	}
	if (288 < transform_.position_.x + I_W_F) {
		transform_.position_.x = 288 - I_W_F;
	}
	if (transform_.position_.x - I_W_F < -288) {
		transform_.position_.x = -288 + I_W_F;
	}

	// 視界が狭まっていく
	visibility_val_ -= VISIBILLITY_VAL_SUB;

	if (is_recovery_visibility_) {
		visibility_recovery_count_ += VISIBILLITY_RECOVERY_FRAME;
		if (VISIBILLITY_VAL_RECOVERY < visibility_recovery_count_) {
			visibility_recovery_count_ = 0;
			supply_count_ -= 1;
			if (supply_count_ == 0) {
				is_recovery_visibility_ = false;
			}
		}
		else {
			visibility_val_ += VISIBILLITY_RECOVERY_FRAME;
		}
	}

	visibility_val_ = std::clamp(visibility_val_, VISIBILLITY_VAL_MIN, VISIBILLITY_VAL_MAX);

	if (visibility_val_ <= VISIBILLITY_VAL_MIN) {
		// ゲームオーバー
		// new Play();
	}

	/*
	
	各障害物のupdate内に追加、Supplyにも
	スクリーンの1.5倍の範囲外に行ったら消す

	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
	*/
}

void Player::render() {
	SetDrawBlendMode(blend_mode_, blend_parameter_);
	if (is_render_player_) {
		image_.render(transform_);
	}
	visibility_.render(transform_.position_.x, transform_.position_.y, visibility_val_, 0);
}

void Player::hit(GameObject& other, const int direction) {

	if (!is_hit_obstacle_) {
		// 障害物に当たるたびに残機が1減る
		if (other.getTypeId() == Object::getTypeId<Obstacle>() ||
			other.getTypeId() == Object::getTypeId<Meteorite>() ||
			other.getTypeId() == Object::getTypeId<Starfish>() ||
			other.getTypeId() == Object::getTypeId<Ufo>()) {
			is_hit_obstacle_ = true;
			hp_ -= 1;
			// 残機が0になったら仮でPlayシーンのリロード
			if (hp_ == 0) {
				new Play();
			}
			seq_.change(&Player::seqHitObstacle);
		}
	}

	if (other.getTypeId() == Object::getTypeId<Supply>()) {
		is_recovery_visibility_ = true;
		supply_count_ += 1;
		other.destory();
	}

	if (other.getTypeId() == Object::getTypeId<Armor>()) {
		if (hp_ < MAX_HP) {
			hp_ += 1;
		}
		other.destory();
	}

	if (other.getTypeId() == Object::getTypeId<CollectionItem>()) {
		collection_ += 1;
		other.destory();
	}
}
