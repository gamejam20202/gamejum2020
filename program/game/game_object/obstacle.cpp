#include "obstacle.h"


Obstacle::Obstacle(const Image::DivInfo& div_info, Transform& transform) : GameObject(transform), image_(div_info) {
	collider_ = new Collider(image_.getWidth(), image_.getHeight());
	setLayer(1);
}

Obstacle::Obstacle(const char* file_name, Transform& transform) : GameObject(transform), image_(Image(file_name)) {
	collider_ = new Collider(image_.getWidth(), image_.getHeight());
	setLayer(1);
}

Obstacle::~Obstacle() {
	delete collider_;
}

// 更新
void Obstacle::update(const float delta_time) {
	image_.update(delta_time);
	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
}

// 描画
void Obstacle::render() {
	image_.render(transform_);
}

// 他のCollider付きのGameObjectに当たった時の処理
void Obstacle::hit(GameObject& other, const int direction) {

}
