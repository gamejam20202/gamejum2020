#include "ufo.h"
#include <time.h>
#define PI 3.141593
#define RIGHT 0
#define LEFT 1

Ufo::Ufo(const Image::DivInfo& div_info, Transform& transform, const int around) : Obstacle(div_info, transform) {
	this->around = around;
	sin_as_count = (1 + rand() % 10);
	move_x = (2 + rand() % 5);
	sin_as_count = sin_as_count / 10;
	move_x = move_x / 5;
}

Ufo::~Ufo() {
}

// 更新
void Ufo::update(const float delta_time) {
	image_.update(delta_time);

	time_count += delta_time;
	transform_.position_.y+= sin(PI * time_count / sin_as_count) * 5.0f;
	if (around == RIGHT) {
		transform_.position_.x += move_x;
	}
	if (around == LEFT) {
		transform_.position_.x -= move_x;
	}
	if (!GameManager::getInstance()->getNowScene()->camera_.isInScreenRange(transform_.position_, 0.5f)) {
		destory();
	}
}

// 描画
void Ufo::render() {
	image_.render(transform_);
}

// 他のCollider付きのGameObjectに当たった時の処理
void Ufo::hit(GameObject& other, const int direction) {

}