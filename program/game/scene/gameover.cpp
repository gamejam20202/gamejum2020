#include "gameover.h"
#include "title.h"
#include "play.h"

bool Gameover::seqInit(const float delta_time) {

	Sprite* sprite = new Sprite("graphics/gameover_back.png");  //画像未指定
	sprite->setAffectCamera2DRate(0);

	new Text(String("Gameover"), t2k::Vector3(-100, -300, 0), 50);
	new Text(String("Enterキーでタイトルへ"), t2k::Vector3(-500, 360, 0));
	new Text(String("Spaceキーでリスタート"), t2k::Vector3(100, 360, 0));
	seq_.change(&Gameover::seqIdle);

	return true;
}

bool Gameover::seqIdle(const float delta_time) {

	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_RETURN)) {
		new Title();
	}
	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_SPACE)) {
		new Play();
	}

	return true;
}

void Gameover::updateSequance(const float delta_time) {
	seq_.update(delta_time);
}
