#pragma once
#include "../../engine/engine.h"


class Result : public Scene {
public:
	Result() {}
	~Result() {}
	//====================================================================================================
// メンバ変数
	t2k::Sequence<Result*> seq_ = t2k::Sequence<Result*>(this, &Result::seqInit);		// シーケンス処理

	Button* go_title_ = nullptr;

	//====================================================================================================
	// メンバ関数

	// シーケンス
	bool seqInit(const float delta_time);			// 初期化
	bool seqIdle(const float delta_time);			// 通常

	void updateSequance(const float delta_time) override;

};