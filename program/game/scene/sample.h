#pragma once
#include "../../engine/engine.h"


class Sample : public Scene {
private:
	//====================================================================================================
	// メンバ変数
	t2k::Sequence<Sample*> seq_ = t2k::Sequence<Sample*>(this, &Sample::seqInit);		// シーケンス処理

	//====================================================================================================
	// メンバ関数

	// シーケンス
	bool seqInit(const float delta_time);			// 初期化
	bool seqFadeIn(const float delta_time);			// フェードイン
	bool seqIdle(const float delta_time);			// 通常
	bool seqFadeOut(const float delta_time);		// フェードアウト

	// シーケンスのupdate
	void updateSequance(const float delta_time) final override;
public:
	//====================================================================================================
	// コンストラクタ
	Sample() {}

	//====================================================================================================
	// デストラクタ
	~Sample() {}
};
