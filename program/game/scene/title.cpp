#include "title.h"
#include "play.h"
#include "tutorial.h"


bool Title::seqInit(const float delta_time) {

	new Text(String("Title"), t2k::Vector3(-100, -300, 0), 50);
	new Text(String("Enterキーでスタート"), t2k::Vector3(-200, 0, 0));

	seq_.change(&Title::seqIdle);

	return true;
}

bool Title::seqIdle(const float delta_time) {

	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_RETURN)) {
		new Play();
		//new Tutorial();
	}

	return true;
}

void Title::updateSequance(const float delta_time) {
	seq_.update(delta_time);
}
