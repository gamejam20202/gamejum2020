#pragma once
#include "../../engine/engine.h"

class Player;
class RightUi;
class LeftUi;

class Tutorial : public Scene {
public:
	Player* player = nullptr;
	RightUi* right_ui_ = nullptr;
	LeftUi* left_ui_ = nullptr;
	float time_count_ = 0;
	const float BG_AFFECT = 0.25f;

	Tutorial() {}
	~Tutorial() {}

	//====================================================================================================
	// メンバ変数
	t2k::Sequence<Tutorial*> seq_ = t2k::Sequence<Tutorial*>(this, &Tutorial::seqInit);		// シーケンス処理

	//====================================================================================================
	// メンバ関数

	// シーケンス
	bool seqInit(const float delta_time);// 初期化
	bool Tuto(const float delta_time);
	bool seqIdle(const float delta_time);			// 通常

	void updateSequance(const float delta_time) override;
};
