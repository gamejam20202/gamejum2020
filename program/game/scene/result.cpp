#include "result.h"
#include "../game_object/obstacle.h"
#include "../../engine/object/game_object/button.h"
#include "../../engine/object/game_object/mouse_cursor.h"
#include "title.h"


bool Result::seqInit(const float delta_time) {

	Sprite* sprite = new Sprite("graphics/gameover_back.png");  //画像未指定
	sprite->setAffectCamera2DRate(0);

	new Text(String("Gameover"), t2k::Vector3(-100, -300, 0), 50);
	new Text(String("ボタンを押してタイトルへ"), t2k::Vector3(-500, 360, 0));

	go_title_ = new Button("graphics/red1.bmp", String(""), t2k::Vector3(0, 300, 0));

	seq_.change(&Result::seqIdle);

	return true;
}

bool Result::seqIdle(const float delta_time) {

	if (go_title_->onMouseStay()) {
		if (isInputMouseDownTrigger(MouseCursor::MOUSECODE_LEFT)) {
			new Title();
		}
	}

	return true;
}

void Result::updateSequance(const float delta_time) {
	seq_.update(delta_time);

	GameManager::getInstance()->debug_.log("リザルト画面");
}

/*
bool Gameover::seqInit(const float delta_time) {

	Sprite* sprite = new Sprite("graphics/gameover_back.png");  //画像未指定
	sprite->setAffectCamera2DRate(0);

	new Text(String("Gameover"), t2k::Vector3(-100, -300, 0), 50);
	new Text(String("Enterキーでタイトルへ"), t2k::Vector3(-500, 360, 0));
	new Text(String("Spaceキーでリスタート"), t2k::Vector3(100, 360, 0));
	seq_.change(&Gameover::seqIdle);

	return true;
}

bool Gameover::seqIdle(const float delta_time) {

	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_RETURN)) {
		new Title();
	}
	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_SPACE)) {
		new Play();
	}

	return true;
}

void Gameover::updateSequance(const float delta_time) {
	seq_.update(delta_time);
}
*/