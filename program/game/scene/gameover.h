#pragma once
#include "../../engine/engine.h"


class Gameover : public Scene {
public:
	Gameover() {}
	~Gameover() {}

	//====================================================================================================
	// メンバ変数
	t2k::Sequence<Gameover*> seq_ = t2k::Sequence<Gameover*>(this, &Gameover::seqInit);		// シーケンス処理

	//====================================================================================================
	// メンバ関数

	// シーケンス
	bool seqInit(const float delta_time);			// 初期化
	bool seqIdle(const float delta_time);			// 通常

	void updateSequance(const float delta_time) override;
};