#include "play.h"
#include "../game_object/player.h"
#include "../game_object/obstacle.h"
#include "../game_object/ufo.h"
#include "../game_object/meteorite.h"
#include "../game_object/Starfish.h"
#include "../game_object/supply.h"
#include "../game_object/armor.h"
#include "../game_object/left_ui.h"
#include "../game_object/right_ui.h"


bool Play::seqInit(const float delta_time) {

	const int BG_H = 3840;
	const int S_H = camera_.getScreenHeight();

	const int R = -(BG_H >> 1) + (S_H >> 1);

	Sprite* bg = new Sprite("graphics/bg/haikei wakuseiari.png", Transform(t2k::Vector3(0, R, 0)));
	bg->setAffectCamera2DRate(BG_AFFECT);
	player = new Player(Transform(t2k::Vector3(0, 200, 0)));

	right_ui_ = new RightUi(player, Transform(t2k::Vector3(375 + 25, 0, 0)));
	left_ui_ = new LeftUi(player, Transform(t2k::Vector3(-375 - 25, 0, 0)));

	seq_.change(&Play::seqIdle);

	return true;
}

bool Play::seqIdle(const float delta_time) {

	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_M)) {
		right_ui_->message_ = new String("ここにメッセージが\nはいるよ\na\na");
		right_ui_->speech_bubble_ = new Image("graphics/op/op_serifu.png");
	}

	if (right_ui_->message_) {
		if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_D)) {
			delete right_ui_->message_;
			right_ui_->message_ = nullptr;
			delete right_ui_->speech_bubble_;
			right_ui_->speech_bubble_ = nullptr;
		}
	}

	int y_int = camera_.transform_.position_.y;

	// 
	int obstacle_y = y_int % 100;

	if (obstacle_y == 0) {

		const int PLAY_SCREEN_LEFT_LIMIT = -288;

		float x = PLAY_SCREEN_LEFT_LIMIT + 60 + (rand() % (576 - 120));

		const int OBSTACLE = 0;
		const int UFO = 1;
		const int METERITE = 2;
		const int STARFHISH = 3;

		int select = rand() % 4;
		//int select = STARFHISH;

		if (select == OBSTACLE) {

			const int MAX_GRAPH = 7;

			char* file_name[MAX_GRAPH] = {
				"graphics/obstacle/icon_gomi1.png",
				"graphics/obstacle/icon_gomi2.png", 
				"graphics/obstacle/icon_gomi3.png",
				"graphics/obstacle/icon_gomi4.png",
				"graphics/obstacle/icon_gomi5.png",
				"graphics/obstacle/icon_gomi6.png",
				"graphics/obstacle/icon_gomi7.png",
			};

			int r = rand() % MAX_GRAPH;

			new Obstacle(file_name[r], Transform(t2k::Vector3(x, y_int - (camera_.getScreenHeight() >> 1), 0)));
		}
		else if (select == UFO) {

			int ufo_direction = rand() % 2;

			Image::DivInfo info = { "graphics/obstacle/icon_ufo3.png", 2, 2, 1, 94, 84 };

			new Ufo(info, Transform(t2k::Vector3(-150, y_int - (camera_.getScreenHeight() >> 1), 0)), ufo_direction);
		}
		else if (select == METERITE) {

			const int MAX_GRAPH = 6;

			char* file_name[MAX_GRAPH] = {
				"graphics/obstacle/icon_inseki1.png",
				"graphics/obstacle/icon_inseki2.png",
				"graphics/obstacle/icon_inseki3.png",
				"graphics/obstacle/icon_inseki4.png",
				"graphics/obstacle/icon_inseki5.png",
				"graphics/obstacle/icon_inseki6.png",
			};

			int r = rand() % MAX_GRAPH;

			new Meteorite(file_name[r], Transform(t2k::Vector3(x, y_int - (camera_.getScreenHeight() >> 1), 0)));
		}
		else if (select == STARFHISH) {
			
			new Starfish("graphics/obstacle/hitode.png", Transform(t2k::Vector3(x, y_int - (camera_.getScreenHeight() >> 1), 0)), player, GameManager::getInstance());
		}
	}

	int item_y = y_int % 200;

	if (item_y == 0) {

		const int PLAY_SCREEN_LEFT_LIMIT = -288;

		float x = PLAY_SCREEN_LEFT_LIMIT + 60 + (rand() % (576 - 120));

		int select = rand() % 4;

		if (select < 3) {
			new Supply("graphics/item/icon_enekaifuku.png", Transform(t2k::Vector3(x, camera_.transform_.position_.y - (camera_.getScreenHeight() >> 1), 0)));
		}
		else {
			new Armor("graphics/item/icon_kaifuku.png", Transform(t2k::Vector3(x, camera_.transform_.position_.y - (camera_.getScreenHeight() >> 1), 0)));
		}
	}

	return true;
}

void Play::updateSequance(const float delta_time) {
	seq_.update(delta_time);
	camera_.transform_.position_.y -= 1;
	player->transform_.position_.y -= 0.2f;
	// GameManager::getInstance()->debug_.log("HP: %d", player->hp_);
}
