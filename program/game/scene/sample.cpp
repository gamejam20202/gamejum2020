#include "sample.h"
#include "title.h"


//====================================================================================================
// 初期化シーケンス
bool Sample::seqInit(const float delta_time) {

	//------------------//
	// オブジェクト生成 //
	//------------------//
	new Text(String("Sample Scene"), t2k::Vector3(0, -300, 0));
	new Text(String("Enter キーでTitleへ"), t2k::Vector3(-100, 0, 0));

	seq_.change(&Sample::seqFadeIn);

	return true;
}

//====================================================================================================
// フェードインシーケンス
bool Sample::seqFadeIn(const float delta_time) {

	if (seq_.isStart()) {
		transition_->setIncrasePerFrame(20);
		transition_->begin(Transition::TRANSMODE_SUB);
	}

	if (transition_->isEnd()) {
		seq_.change(&Sample::seqIdle);
	}

	return true;
}

//====================================================================================================
// 通常シーケンス
bool Sample::seqIdle(const float delta_time) {

	// シーン切り替え条件
	if (t2k::Input::isKeyDownTrigger(t2k::Input::KEYBORD_RETURN)) {
		seq_.change(&Sample::seqFadeOut);
	}

	return true;
}

//====================================================================================================
// フェードアウトシーケンス
bool Sample::seqFadeOut(const float delta_time) {

	if (seq_.isStart()) {
		transition_->setIncrasePerFrame(20);
		transition_->begin(Transition::TRANSMODE_ADD);
	}

	if (transition_->isEnd()) {
		//----------------//
		// シーン切り替え //
		//----------------//
		new Title();
	}

	return true;
}

//====================================================================================================
// シーケンスのupdate
void Sample::updateSequance(const float delta_time) {
	seq_.update(delta_time);
}
