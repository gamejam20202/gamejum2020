#include "DxLib.h"
#include "../library/t2klib.h"
#include "../support/Support.h"
#include "game_main.h"
#include "../engine/engine.h"


void gameMain( float deltatime ) {
	GameManager* mgr = GameManager::getInstance();
	mgr->update(deltatime);
	mgr->render();
}
