#pragma once
#include "game_manager.h"

#include "my_library/collider.h"
#include "my_library/receive_gravity.h"
#include "my_library/normalize.h"
#include "my_library/string_.h"

#include "scene/scene.h"

#include "object/game_object/sprite.h"
#include "object/game_object/text.h"
#include "object/game_object/button.h"
