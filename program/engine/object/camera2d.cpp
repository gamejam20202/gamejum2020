#include "camera2d.h"


//====================================================================================================
// コンストラクタ
Camera2D::Camera2D(const int screen_width, const int screen_height)
	: SCREEN_WIDTH(screen_width)
	, SCREEN_HEIGHT(screen_height) 
{}

//====================================================================================================
// スクリーン範囲 + range 内に position が入っているかどうか
bool Camera2D::isInScreenRange(const t2k::Vector3& position, float range) {
	if (position.y < transform_.position_.y - (SCREEN_HEIGHT >> 1) - SCREEN_HEIGHT * range) return false;
	if (position.y > transform_.position_.y + (SCREEN_HEIGHT >> 1) + SCREEN_HEIGHT * range) return false;
	if (position.x < transform_.position_.x - (SCREEN_WIDTH >> 1)  - SCREEN_WIDTH  * range) return false;
	if (position.x > transform_.position_.x + (SCREEN_WIDTH >> 1)  + SCREEN_WIDTH  * range) return false;
	return true;
}

//====================================================================================================
// 描画する際に追加する座標
t2k::Vector3 Camera2D::getRenderPosition(const float affect_rate) {
	float x = (SCREEN_WIDTH >> 1)  - transform_.position_.x * affect_rate;
	float y = (SCREEN_HEIGHT >> 1) - transform_.position_.y * affect_rate;
	return t2k::Vector3(x, y, 0);
}
