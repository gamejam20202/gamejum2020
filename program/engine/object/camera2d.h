#pragma once
#include "object.h"
#include "../my_library/transform_.h"


class Camera2D : public Object {
private:
	//====================================================================================================
	// メンバ変数
	const int SCREEN_WIDTH;		// スクリーンの幅
	const int SCREEN_HEIGHT;	// スクリーンの高さ

public:
	//====================================================================================================
	// コンストラクタ
	Camera2D(const int screen_width, const int screen_height);

	//====================================================================================================
	// デストラクタ
	~Camera2D() {}

	//====================================================================================================
	// メンバ変数
	Transform transform_;		// 座標、回転、大きさ

	//====================================================================================================
	// メンバ関数

	// スクリーン範囲 + range 内に position が入っているかどうか
	bool isInScreenRange(const t2k::Vector3& position, float range = 0.0f);

	// 描画する際に追加する座標
	t2k::Vector3 getRenderPosition(const float affect_rate);

	// ゲッター
	inline int getScreenWidth() { return SCREEN_WIDTH; }
	inline int getScreenHeight() { return SCREEN_HEIGHT; }
};
