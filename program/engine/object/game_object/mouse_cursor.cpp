#include "DxLib.h"
#include "../../my_library/collider.h"
#include "mouse_cursor.h"


//====================================================================================================
// コンストラクタ
MouseCursor::MouseCursor(const int screen_width, const int screen_height, const int layer)
	: SCREEN_WIDTH_HALF(screen_width >> 1)
	, SCREEN_HEIGHT_HALF(screen_height >> 1) {
	collider_ = new Collider(2, 2);
	collider_->setIsTrigger(true);
	setAffectCamera2DRate(0);
	setLayer(layer);
}

//====================================================================================================
// デストラクタ
MouseCursor::~MouseCursor() {
	delete collider_;
}

//====================================================================================================
// 更新
void MouseCursor::update(const float delta_time) {

	input_previous_ = input_now_;
	input_now_ = GetMouseInput();

	position_previous_ = transform_.position_;

	int x, y;
	GetMousePoint(&x, &y);
	transform_.position_.x = x - SCREEN_WIDTH_HALF;
	transform_.position_.y = y - SCREEN_HEIGHT_HALF;
}

////====================================================================================================
//// 描画
//void MouseCursor::render() {
//	SetDrawBlendMode(blend_mode_, blend_parameter_);
//	image_.render(transform_);
//}
