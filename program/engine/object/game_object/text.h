#pragma once
#include "DxLib.h"
#include "../../my_library/string_.h"
#include "game_object.h"


class Text : public GameObject {
private:
	//====================================================================================================
	// メンバ変数
	int font_size_;					// フォントサイズ
	int color_ = 0xFFFFFFFF;		// 文字の色
	int interval_ = 0;				// 行間

public:
	//====================================================================================================
	// コンストラクタ
	Text(const String& string = String(), const t2k::Vector3& position = t2k::Vector3(0, 0, 0), const int font_size = 20);

	//====================================================================================================
	// デストラクタ
	~Text() {}

	//====================================================================================================
	// メンバ変数
	String string_;		// 改行ありの文字列

	//====================================================================================================
	// メンバ関数

	// 描画
	void render() final override;

	// セッター
	inline void setDrawBlendMode(const int blend_mode, const int blend_parameter) {
		blend_mode_ = blend_mode;
		blend_parameter_ = blend_parameter;
	}
	inline void setFontSize(const int font_size) { font_size_ = font_size; }
	inline void setColor(const int color) { color_ = color; }
	inline void setInterval(const int interval) { interval_ = interval; }
};
