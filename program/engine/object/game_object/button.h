#pragma once
#include "../../my_library/image.h"
#include "../../my_library/string_.h"
#include "game_object.h"


class Button : public GameObject {
private:
	//====================================================================================================
	// メンバ変数
	Image image_;					// 画像
	String string_;					// 文字
	int font_size_ = 20;			// 文字のサイズ
	int color_ = 0xFFFFFFFF;		// 文字の色
	bool stay_now_ = false;			// 現在のマウス進入状況
	bool stay_previous_ = false;	// 1フレーム前のマウス進入状況

public:
	//====================================================================================================
	// コンストラクタ
	Button(const char* file_name, const String& string = String("Button"), const t2k::Vector3& position = t2k::Vector3(0, 0, 0));

	//====================================================================================================
	// デストラクタ
	~Button();

	//====================================================================================================
	// メンバ関数

	// 更新
	void update(const float delta_time) final override;
	
	// 描画
	void render() final override;
	
	// 他のCollider付きのGameObjectに当たった時の処理
	void hit(GameObject& other, const int direction) final override;

	// マウスが進入したときに1フレームだけtrue
	inline bool onMouseEnter() { return stay_now_ && !stay_previous_; }
	
	// マウスが重なっている間true
	inline bool onMouseStay() { return stay_now_; }
	
	// マウスが出て行ったときに1フレームだけtrue
	inline bool onMouseExit() { return !stay_now_ && stay_previous_; }

	// セッター
	inline void setFontSize(const int font_size) { font_size_ = font_size; }
	inline void setColor(const int color) { color_ = color; }
};
