#include "DxLib.h"
#include "transition.h"


//====================================================================================================
// コンストラクタ
Transition::Transition(const int screen_width, const int screen_height, const int layer)
	: SCREEN_WIDTH(screen_width)
	, SCREEN_HEIGHT(screen_height) {
	setAffectCamera2DRate(0);
	setLayer(layer);
}

//====================================================================================================
// トランジション開始
void Transition::begin(const TransitionMode transition_mode) {
	transition_mode_ = transition_mode;
	is_execution_ = true;
}

//====================================================================================================
// 終了したときに1フレームだけtrueが帰ってくる
bool Transition::isEnd() {
	return is_end_;
}

//====================================================================================================
// 更新
void Transition::update(const float delta_time) {
	if (is_end_) is_end_ = false;
	if (!is_execution_) return;
	blend_parameter_ += (transition_mode_ * incrase_per_frame_);
	if (blend_parameter_ <= 0 || 255 <= blend_parameter_) {
		setBlendParameter(blend_parameter_);
		is_execution_ = false;
		is_end_ = true;
		return;
	}
}

//====================================================================================================
// 描画
void Transition::render() {
	if (blend_parameter_ <= 0) return;
	SetDrawBlendMode(blend_mode_, blend_parameter_);
	DrawFillBox(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, color_);
}
