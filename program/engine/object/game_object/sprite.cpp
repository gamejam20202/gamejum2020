#include "sprite.h"


//====================================================================================================
// コンストラクタ
Sprite::Sprite(const char* file_name, const Transform& transform)
	: GameObject(transform)
	, image_(Image(file_name))
{}

//====================================================================================================
// コンストラクタ
Sprite::Sprite(const Image::DivInfo& div_info, const Transform& transform)
	: GameObject(transform)
	, image_(Image(div_info))
{}

//====================================================================================================
// 更新
void Sprite::update(const float delta_time) {
	image_.update(delta_time);
}

//====================================================================================================
// 描画
void Sprite::render() {
	SetDrawBlendMode(blend_mode_, blend_parameter_);
	image_.render(transform_);
}
