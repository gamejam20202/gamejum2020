#pragma once
#include "../../my_library/image.h"
#include "game_object.h"


class MouseCursor : public GameObject {
public:
	enum MouseCode {
		MOUSECODE_LEFT,		// マウス左ボタン
		MOUSECODE_RIGHT,	// マウス右ボタン
		MOUSECODE_MIDDLE,	// マウス中央ボタン
		MOUSECODE_1,		// マウス１ボタン
		MOUSECODE_2,		// マウス２ボタン
		MOUSECODE_3,		// マウス３ボタン
		MOUSECODE_4,		// マウス４ボタン
		MOUSECODE_5,		// マウス５ボタン
		MOUSECODE_6,		// マウス６ボタン
		MOUSECODE_7,		// マウス７ボタン
		MOUSECODE_8,		// マウス８ボタン
		MOUSECODE_MAX
	};

private:
	//====================================================================================================
	// メンバ変数
	// Image image_ = Image("graphics/cursor.png");	// カーソルの画像
	int input_now_ = 0;					// 今の入力状況
	int input_previous_ = 0;			// 1フレーム前の入力状況
	t2k::Vector3 position_previous_;	// 1フレーム前の位置
	const int SCREEN_WIDTH_HALF;		// スクリーンの幅の半分
	const int SCREEN_HEIGHT_HALF;		// スクリーンの高さの半分
	// マウス入力コード
	const int MOUSE_CODES[MOUSECODE_MAX]{
		MOUSE_INPUT_LEFT,		// マウス左ボタン
		MOUSE_INPUT_RIGHT,		// マウス右ボタン
		MOUSE_INPUT_MIDDLE,		// マウス中央ボタン
		MOUSE_INPUT_1,			// マウス１ボタン
		MOUSE_INPUT_2,			// マウス２ボタン
		MOUSE_INPUT_3,			// マウス３ボタン
		MOUSE_INPUT_4,			// マウス４ボタン
		MOUSE_INPUT_5,			// マウス５ボタン
		MOUSE_INPUT_6,			// マウス６ボタン
		MOUSE_INPUT_7,			// マウス７ボタン
		MOUSE_INPUT_8,			// マウス８ボタン
	};

public:
	//====================================================================================================
	// コンストラクタ
	MouseCursor(const int screen_width, const int screen_height, const int layer);
	
	//====================================================================================================
	// デストラクタ
	~MouseCursor();

	//====================================================================================================
	// メンバ関数

	// 更新
	void update(const float delta_time) final override;

	// 描画
	// void render() final override;

	// 1フレームでのマウス座標の変化量
	inline t2k::Vector3 getPositionChangeQuantity() { return transform_.position_ - position_previous_; }

	// 入力
	inline bool isInputDown(const MouseCode mouse_code) { return (input_now_ & MOUSE_CODES[mouse_code]); }

	// トリガー
	inline bool isInputDownTrigger(const MouseCode mouse_code) { return (input_now_ & MOUSE_CODES[mouse_code]) && (input_now_ & MOUSE_CODES[mouse_code]) != (input_previous_ & MOUSE_CODES[mouse_code]); }

	// リリーストリガー
	inline bool isInputReleaseTrigger(const MouseCode mouse_code) { return (input_previous_ & MOUSE_CODES[mouse_code]) && (input_now_ & MOUSE_CODES[mouse_code]) != (input_previous_ & MOUSE_CODES[mouse_code]); }

	// ホイール回転量（1フレームでの回転量 手前が−、奥が＋）
	inline int getWheelRotation() { return GetMouseWheelRotVol(); }
};
