#include "../../my_library/collider.h"
#include "button.h"
#include "mouse_cursor.h"


//====================================================================================================
// コンストラクタ
Button::Button(const char* file_name, const String& string, const t2k::Vector3& position)
	: GameObject(Transform(position))
	, image_(Image(file_name))
	, string_(string) {
	collider_ = new Collider(image_.getWidth(), image_.getHeight(), false, true);
}

//====================================================================================================
// デストラクタ
Button::~Button() {
	delete collider_;
}

//====================================================================================================
// 更新
void Button::update(const float delta_time) {

	transform_.scale_.z = onMouseStay() ? 1.1f : 1.0f;

	stay_previous_ = stay_now_;
	stay_now_ = false;
}

//====================================================================================================
// 描画
void Button::render() {
	SetDrawBlendMode(blend_mode_, blend_parameter_);
	image_.render(transform_);

	int x = transform_.position_.x - ((font_size_ >> 1) * string_.length() * 0.5f);
	int y = transform_.position_.y - (font_size_ >> 1);

	string_.render(x, y, font_size_, color_);
}

//====================================================================================================
// 他のCollider付きのGameObjectに当たった時の処理
void Button::hit(GameObject& other, const int direction) {
	if (other.getTypeId() == Object::getTypeId<MouseCursor>()) {
		stay_now_ = true;
	}
}
