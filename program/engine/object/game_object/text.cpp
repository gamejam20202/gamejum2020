#include "text.h"


//====================================================================================================
// コンストラクタ
Text::Text(const String& string, const t2k::Vector3& position, const int font_size)
	: GameObject(Transform(position))
	, string_(string)
	, font_size_(font_size) {
	setAffectCamera2DRate(0);
}

//====================================================================================================
// 描画
void Text::render() {
	SetDrawBlendMode(blend_mode_, blend_parameter_);
	string_.render(transform_, font_size_, color_, interval_);
}
