#pragma once
#include <algorithm>
#include "DxLib.h"
#include "../../my_library/transform_.h"
#include "../object.h"


class Collider;
class ReceiveGravity;

class GameObject : public Object {
private:
	//====================================================================================================
	// メンバ変数
	int layer_ = 0;							// 描画順 小さいほうが先に描画される
	float affect_camera2d_rate_ = 1;		// カメラ2Dの影響割合（1.0〜0.0）

protected:
	//====================================================================================================
	// メンバ変数
	int blend_mode_ = DX_BLENDMODE_ALPHA;		// ブレンドモード
	int blend_parameter_ = 255;					// 透明度

public:
	//====================================================================================================
	// コンストラクタ
	GameObject(const Transform& transform = Transform());

	//====================================================================================================
	// デストラクタ
	virtual ~GameObject() {}

	//====================================================================================================
	// メンバ変数
	Transform transform_;							// 座標、回転、大きさ
	Collider* collider_ = nullptr;					// 当たり判定
	ReceiveGravity* receive_gravity_ = nullptr;		// 重力

	//====================================================================================================
	// メンバ関数

	// 更新
	virtual void update(const float delta_time) {}

	// 描画
	virtual void render() {}

	// 他のCollider付きのGameObjectに当たった時の処理
	virtual void hit(GameObject& other, const int direction) {}

	// レイヤーの設定
	void setLayer(const int layer);

	// カメラ2Dの影響割合の設定（1.0〜0.0）
	inline void setAffectCamera2DRate(const float affect_camera2d_rate) { affect_camera2d_rate_ = std::clamp(affect_camera2d_rate, 0.0f, 1.0f); }

	// カメラ2Dの影響割合の取得
	inline float getAffectCamera2DRate() { return affect_camera2d_rate_; }

	//====================================================================================================
	// 静的メンバ関数

	// レイヤーの比較（ソート用）
	static inline bool compareLayer(const GameObject* obj1, const GameObject* obj2) { return obj1->layer_ < obj2->layer_; }
};
