#include "../../game_manager.h"
#include "../../scene/scene.h"
#include "game_object.h"


//====================================================================================================
// コンストラクタ
GameObject::GameObject(const Transform& transform)
	: transform_(transform) {
	Scene* scene = GameManager::getInstance()->getNowScene();
	scene->addGameObject(this);
}

//====================================================================================================
// レイヤーの設定
void GameObject::setLayer(const int layer) {
	layer_ = layer;
	GameManager::getInstance()->getNowScene()->sortGameObject();
}
