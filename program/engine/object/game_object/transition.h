#pragma once
#include "game_object.h"


class Transition : public GameObject {
public:
	//====================================================================================================
	// 列挙定数
	enum TransitionMode {
		TRANSMODE_ADD = 1,	// 加算
		TRANSMODE_SUB = -1	// 減算
	};

private:
	//====================================================================================================
	// メンバ変数
	int color_ = 0xFF000000;							// 色
	TransitionMode transition_mode_ = TRANSMODE_SUB;	// 遷移モード

	int incrase_per_frame_ = 10;	// 1フレーム当たりの透明度増加量

	bool is_execution_ = false;		// 実行中フラグ
	bool is_end_ = false;			// 終了フラグ

	const int SCREEN_WIDTH;			// スクリーンの幅
	const int SCREEN_HEIGHT;		// スクリーンの高さ

public:
	//====================================================================================================
	// コンストラクタ
	Transition(const int screen_width, const int screen_height, const int layer);
	
	//====================================================================================================
	// デストラクタ
	~Transition() {}

	//====================================================================================================
	// メンバ関数

	// トランジション開始
	void begin(const TransitionMode transition_mode);
	
	// 終了したときに1フレームだけtrueが帰ってくる
	bool isEnd();

	// 更新
	void update(const float delta_time) final override;

	// 描画
	void render() final override;

	// セッター
	inline void setColor(const int color) { color_ = color; }
	inline void setBlendParameter(const int blend_parameter) { blend_parameter_ = std::clamp(blend_parameter, 0, 255); }
	inline void setIncrasePerFrame(const int incrase_per_frame) { incrase_per_frame_ = incrase_per_frame; }
};
