#pragma once
#include "../../my_library/image.h"
#include "game_object.h"


class Sprite : public GameObject {
private:
	//====================================================================================================
	// メンバ変数
	bool is_turn_ = false;		// 反転フラグ

public:
	//====================================================================================================
	// コンストラクタ
	Sprite(const char* file_name, const Transform& transform = Transform());

	//====================================================================================================
	// コンストラクタ
	Sprite(const Image::DivInfo& div_info, const Transform& transform = Transform());

	//====================================================================================================
	// デストラクタ
	~Sprite() {}

	//====================================================================================================
	// メンバ変数
	Image image_;		// 画像

	//====================================================================================================
	// メンバ関数

	// 更新
	void update(const float delta_time) final override;

	// 描画
	void render() final override;

	// セッター
	inline void setDrawBlendMode(const int blend_mode, const int blend_parameter) {
		blend_mode_ = blend_mode;
		blend_parameter_ = blend_parameter;
	}
	inline void setTurn(const bool is_turn) { is_turn_ = is_turn; }
};
