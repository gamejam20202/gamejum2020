#pragma once
#include <typeinfo>


class Object {
private:
	//====================================================================================================
	// メンバ変数
	unsigned int type_id_ = 0;	// type_info.hash_code()の値
	bool is_alive_ = true;		// 生存フラグ

public:
	//====================================================================================================
	// コンストラクタ
	Object() {}

	//====================================================================================================
	// デストラクタ
	virtual ~Object() {}

	//====================================================================================================
	// メンバ関数

	// 自身のtype_info.hash_code()の値を取得
	inline unsigned int getTypeId() {
		if (type_id_ == 0) {
			type_id_ = typeid(*this).hash_code();
		}
		return type_id_;
	}

	// 生存フラグの取得
	inline bool isAlive() { return is_alive_; }

	// 自身の生存フラグを折る
	inline void destory() { is_alive_ = false; }

	//====================================================================================================
	// 静的メンバ関数

	// テンプレート引数に与えたクラスのtype_info.hash_code()の値を取得
	template<class T> inline static unsigned int getTypeId() { return typeid(T).hash_code(); }
};
