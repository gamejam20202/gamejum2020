#pragma once
#include <list>
#include "../object/camera2d.h"
#include "../object/game_object/transition.h"
#include "../object/game_object/mouse_cursor.h"


class GameObject;

class Scene {
private:
	//====================================================================================================
	// メンバ変数
	std::list<GameObject*> game_objects_;		// シーンに存在するゲームオブジェクトすべての管理用リスト
	float gravity_ = 1.0f;						// 重力
	bool is_sort_ = false;						// ソートフラグ

	const int LAYER_MOUSE_CURSOR = 999999;		// レイヤー定数 マウスカーソル
	const int LAYER_TRANSITION = 999998;		// レイヤー定数 画面遷移

protected:
	//====================================================================================================
	// メンバ変数
	Transition* transition_ = nullptr;			// 遷移
	MouseCursor* mouse_cursor_ = nullptr;		// マウスカーソル

	const int LAYER_UI = 999997;				// レイヤー定数 UI

	//====================================================================================================
	// メンバ関数

	// 派生先のシーケンス処理
	virtual void updateSequance(const float delta_time) {}

public:
	//====================================================================================================
	// コンストラクタ
	Scene();

	//====================================================================================================
	// デストラクタ
	virtual ~Scene();

	Camera2D camera_ = Camera2D(1024, 768);		// 2Dの仮想カメラ
//====================================================================================================
	// メンバ関数

	// 更新
	void update(const float delta_time);

	// 描画
	void render();

	// ゲームオブジェクト管理リストにゲームオブジェクトを追加する
	inline void addGameObject(GameObject* game_object) { game_objects_.emplace_back(game_object); }

	// ゲームオブジェクトのレイヤーを変えた時に呼び出す
	inline void sortGameObject() { is_sort_ = true; }

	// セッター
	inline void setGravity(const float gravity) { gravity_ = gravity; }

	// マウスの座標を取得
	inline t2k::Vector3 getMousePosition() { return mouse_cursor_->transform_.position_; }

	// マウス入力
	inline bool isInputMouseDown(const MouseCursor::MouseCode mouse_code) { return mouse_cursor_->isInputDown(mouse_code); }

	// マウストリガー
	inline bool isInputMouseDownTrigger(const MouseCursor::MouseCode mouse_code) { return mouse_cursor_->isInputDownTrigger(mouse_code); }

	// マウスリリーストリガー
	inline bool isInputMouseReleaseTrigger(const MouseCursor::MouseCode mouse_code) { return mouse_cursor_->isInputReleaseTrigger(mouse_code); }

	// マウスホイール回転量（1フレームでの回転量 手前が−、奥が＋）
	inline int getMouseWheelRotation() { return mouse_cursor_->getWheelRotation(); }
};
