#include "../game_manager.h"
#include "../my_library/collider.h"
#include "../my_library/receive_gravity.h"
#include "scene.h"
#include "../object/game_object/game_object.h"


//====================================================================================================
// コンストラクタ
Scene::Scene() {
	GameManager* mgr = GameManager::getInstance();
	mgr->setNextScene(this);
}

//====================================================================================================
// デストラクタ
Scene::~Scene() {
	for (auto obj : game_objects_) {
		delete obj;
	}
}

//====================================================================================================
// 更新
void Scene::update(const float delta_time) {

	// 生存フラグの確認及び削除
	std::list<GameObject*>::iterator it = game_objects_.begin();
	while (it != game_objects_.end()) {
		if (!(*it)->isAlive()) {
			delete (*it);
			it = game_objects_.erase(it);
			continue;
		}
		++it;
	}

	// 遷移、マウスカーソルの生成（最初の1フレームのみ）
	if (!transition_) {
		transition_ = new Transition(camera_.getScreenWidth(), camera_.getScreenHeight(), LAYER_TRANSITION);
		transition_->setBlendParameter(0);
		mouse_cursor_ = new MouseCursor(camera_.getScreenWidth(), camera_.getScreenHeight(), LAYER_MOUSE_CURSOR);
	}

	// 派生先のシーケンス処理
	updateSequance(delta_time);

	// update呼び出し前の座標用
	std::list<t2k::Vector3> before_position;

	// update
	for (auto obj : game_objects_) {
		before_position.emplace_back(obj->transform_.position_);
		obj->update(delta_time);
	}

	// 重力
	for (auto obj : game_objects_) {
		if (!obj->receive_gravity_) continue;
		obj->receive_gravity_->affectedByGravity(gravity_);
	}

	// 当たり判定
	std::list<t2k::Vector3>::iterator bp_it1 = before_position.begin();
	for (std::list<GameObject*>::iterator it1 = game_objects_.begin(); it1 != game_objects_.end(); ++it1, ++bp_it1) {
		if (!(*it1)->collider_) continue;
		std::list<t2k::Vector3>::iterator bp_it2 = bp_it1;
		for (std::list<GameObject*>::iterator it2 = it1; it2 != game_objects_.end(); ++it2, ++bp_it2) {
			if (it1 == it2) continue;
			if (!(*it2)->collider_) continue;
			Collider::hitActionGameObjects(*(*it1), (*bp_it1), *(*it2), (*bp_it2));
		}
	}

	// ソート
	if (is_sort_) {
		game_objects_.sort(GameObject::compareLayer);
		is_sort_ = false;
	}
}

//====================================================================================================
// 描画
void Scene::render() {
	for (auto obj : game_objects_) {
		t2k::Vector3 tmp_position = obj->transform_.position_;
		obj->transform_.position_ += camera_.getRenderPosition(obj->getAffectCamera2DRate());
		obj->render();
		obj->transform_.position_ = tmp_position;
	}
}
