#include "DxLib.h"
#include "transform_.h"
#include "image.h"


//====================================================================================================
// コンストラクタ
Image::Image()
	: handle_(nullptr)
	, all_num_(0)
	, width_(0)
	, height_(0)
{}

//====================================================================================================
// コンストラクタ
Image::Image(const char* file_name)
	: all_num_(1) {
	handle_ = new int[all_num_];
	handle_[0] = LoadGraph(file_name);
	GetGraphSize(handle_[0], &width_, &height_);
}

//====================================================================================================
// コンストラクタ
Image::Image(const char* file_name, const int all_num, const int x_num, const int y_num, const int width, const int height)
	: all_num_(all_num)
	, width_(width)
	, height_(height) {
	loadDivImage(file_name, all_num_, x_num, y_num, width_, height_);
}

//====================================================================================================
// コンストラクタ
Image::Image(const DivInfo& div_info)
	: Image(div_info.getFileName(), div_info.getAllNum(), div_info.getXNum(), div_info.getYNum(), div_info.getWidth(), div_info.getHeight())
{}

//====================================================================================================
// コピーコンストラクタ
Image::Image(const Image& other)
	: all_num_(other.all_num_)
	, width_(other.width_)
	, height_(other.height_)
	, animation_(other.animation_)
	, animation_incrase_(other.animation_incrase_)
	, animation_type_(other.animation_type_)
	, time_count_(other.time_count_)
	, switch_time_(other.switch_time_) {
	handle_ = new int[all_num_];
	for (int i = 0; i < all_num_; ++i) {
		handle_[i] = other.handle_[i];
	}
}

//====================================================================================================
// デストラクタ
Image::~Image() {
	if (handle_) {
		for (int i = 0; i < all_num_; ++i) {
			DeleteGraph(handle_[i]);
		}
		delete[] handle_;
	}
}

//====================================================================================================
// 画像のロード
void Image::loadImage(const char* file_name) {
	if (handle_) {
		for (int i = 0; i < all_num_; ++i) {
			DeleteGraph(handle_[i]);
		}
		delete[] handle_;
	}
	
	all_num_ = 1;
	animation_ = 0;

	handle_ = new int[all_num_];
	handle_[0] = LoadGraph(file_name);
	GetGraphSize(handle_[0], &width_, &height_);
}

//====================================================================================================
// 画像の分割ロード
void Image::loadDivImage(const char* file_name, const int all_num, const int x_num, const int y_num, const int width, const int height) {
	if (handle_) {
		for (int i = 0; i < all_num_; ++i) {
			DeleteGraph(handle_[i]);
		}
		delete[] handle_;
	}
	
	all_num_ = all_num;
	width_ = width;
	height_ = height;
	animation_ = 0;
	animation_incrase_ = 1;
	time_count_ = 0.0f;

	handle_ = new int[all_num_];
	LoadDivGraph(file_name, all_num_, x_num, y_num, width_, height_, handle_);
}

//====================================================================================================
// 更新処理 アニメーションのみ
void Image::update(const float delta_time) {
	if (all_num_ <= 1) return;
	time_count_ += delta_time;
	if (switch_time_ < time_count_) {
		time_count_ = 0;
		animation_ += animation_incrase_;
		if (animation_type_ == ANIMTYPE_LOOP) {
			if (all_num_ <= animation_) {
				animation_ = 0;
			}
		}
		else if (animation_type_ == ANIMTYPE_ROUND_TRIP) {
			if (animation_ <= 0 || all_num_ - 1 <= animation_) {
				animation_incrase_ = -animation_incrase_;
			}
		}
	}
}

//====================================================================================================
// 描画
void Image::render(const Transform& transform, const bool turn) {
	render(transform.position_.x, transform.position_.y, transform.scale_.z, transform.rotation_.z, turn);
}

//====================================================================================================
// 描画
void Image::render(const int x, const int y, const double scale, const double angle, const bool turn) {
	DrawRotaGraph(x, y, scale, t2k::toRadian(angle), handle_[animation_], true, turn);
}

//====================================================================================================
// 分割ロードクラス

//====================================================================================================
// コンストラクタ
Image::DivInfo::DivInfo(const char* file_name, int all_num, int x_num, int y_num, int width, int height)
	: file_name_(file_name)
	, all_num_(all_num)
	, x_num_(x_num)
	, y_num_(y_num)
	, width_(width)
	, height_(height)
{}

//====================================================================================================
// ファイルに書き込み
void Image::DivInfo::fWrite(FILE* fp) {

	c_file_name_index_ = file_name_.length() + 1;

	fwrite(&c_file_name_index_, sizeof(int), 1, fp);

	char* buff = new char[c_file_name_index_];
	for (int i = 0; i < c_file_name_index_; ++i) {
		buff[i] = file_name_[i];
	}

	fwrite(buff, sizeof(char), c_file_name_index_, fp);

	fwrite(&all_num_, sizeof(int), 1, fp);
	fwrite(&x_num_, sizeof(int), 1, fp);
	fwrite(&y_num_, sizeof(int), 1, fp);
	fwrite(&width_, sizeof(int), 1, fp);
	fwrite(&height_, sizeof(int), 1, fp);

	delete[] buff;
}

//====================================================================================================
// ファイルから読み込み
void Image::DivInfo::fRead(FILE* fp) {

	fread(&c_file_name_index_, sizeof(int), 1, fp);

	char* buff = new char[c_file_name_index_];

	fread(buff, sizeof(char), c_file_name_index_, fp);

	fread(&all_num_, sizeof(int), 1, fp);
	fread(&x_num_, sizeof(int), 1, fp);
	fread(&y_num_, sizeof(int), 1, fp);
	fread(&width_, sizeof(int), 1, fp);
	fread(&height_, sizeof(int), 1, fp);

	file_name_ = buff;

	delete[] buff;
}
