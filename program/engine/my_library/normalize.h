#pragma once


class Normalize {
private:
	//====================================================================================================
	// メンバ変数
	float value_ = 0;
	float incrase_per_frame_;			// フレーム当たりの増加量

public:
	//====================================================================================================
	// コンストラクタ
	Normalize(const float incrase_per_frame = 0.1f);

	//====================================================================================================
	// デストラクタ
	~Normalize() {}

	//====================================================================================================
	// メンバ関数

	// UnityのGetAxisのHorizontal, Verticalみたいな感じ
	float getValue(const bool negative, const bool positive);

	// value_を0にする
	inline void resetValue() { value_ = 0; }

	// セッター
	inline void setIncrasePerFrame(const float incrase_per_frame) { incrase_per_frame_ = incrase_per_frame; }
};
