#include "transform_.h"


//====================================================================================================
// コンストラクタ
Transform::Transform(const t2k::Vector3& position, const t2k::Vector3& rotation, const t2k::Vector3& scale)
	: position_	(position)
	, rotation_	(rotation)
	, scale_	(scale)
{}

//====================================================================================================
// 等速直線運動でゴールまで移動する
bool Transform::isMoveTo(const t2k::Vector3& goal, const float speed) {
	t2k::Vector3 next_position = position_ + (goal - position_).normalize() * speed;
	if ((next_position - goal).length() < speed) {
		position_ = goal;
		return true;
	}
	else {
		position_ = next_position;
	}
	return false;
}

//====================================================================================================
// 線形補完をかけてゴールまで移動する
bool Transform::isLerpMoveTo(const t2k::Vector3& goal, const float div) {
	t2k::Vector3 next_position = position_ + (goal - position_) * div;
	if ((next_position - goal).length() < 0.05f) {
		position_ = goal;
		return true;
	}
	else {
		position_ = next_position;
	}
	return false;
}
