#include "DxLib.h"
#include "debug.h"


//====================================================================================================
// 出力したい文字列を設定（フォーマット指定子対応）
void Debug::log(const char* str, ...) {
	char buff[255] = { 0 };
	va_list argptr;

	va_start(argptr, str);
	vsprintf(buff, str, argptr);
	va_end(argptr);
	
	str_ = buff;
	str_ = "Debug::log: " + str_;
	is_render_ = true;
}

//====================================================================================================
// log(char* str, ...)で指定した文字列を画面下部に出力
void Debug::render() {
	if (!is_render_) return;
	
	const int FONT_SIZE = 20;

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);
	DrawFillBox(0, 0, (FONT_SIZE << 2) + str_.length() * (FONT_SIZE >> 1), (FONT_SIZE << 1), 0xFF000000);

	SetFontSize(FONT_SIZE);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
	DrawString(FONT_SIZE, (FONT_SIZE >> 1), str_.c_str(), 0xFFFFFFFF);

	is_render_ = false;
}

//====================================================================================================
// 外部フォルダ「text」にテキストファイルを作成（text/任意のファイル名.txt）（フォーマット指定子対応）
void Debug::printText(const std::string file_path, const char* str, ...) {
	FILE* fp;
	fopen_s(&fp, file_path.c_str(), "w");
	if (fp == nullptr) return;

	char buff[255] = { 0 };
	va_list argptr;

	va_start(argptr, str);
	vsprintf(buff, str, argptr);
	va_end(argptr);

	fprintf_s(fp, buff);
	fclose(fp);
}
