#pragma once
#include "../../library/t2klib.h"


class Transform {
public:
	//====================================================================================================
	// コンストラクタ
	Transform(const t2k::Vector3& position = t2k::Vector3(0, 0, 0), const t2k::Vector3& rotation = t2k::Vector3(0, 0, 0), const t2k::Vector3& scale = t2k::Vector3(1, 1, 1));
	
	//====================================================================================================
	// デストラクタ
	~Transform() {}

	//====================================================================================================
	// メンバ変数
	t2k::Vector3 position_;		// 座標
	t2k::Vector3 rotation_;		// 回転
	t2k::Vector3 scale_;		// 大きさ

	//====================================================================================================
	// メンバ関数

	// 等速直線運動でゴールまで移動する
	bool isMoveTo(const t2k::Vector3& goal, const float speed);

	// 線形補完をかけてゴールまで移動する
	bool isLerpMoveTo(const t2k::Vector3& goal, const float div);
};
