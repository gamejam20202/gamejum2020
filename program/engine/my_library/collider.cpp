#include "collider.h"
#include "receive_gravity.h"
#include "../object/game_object/game_object.h"


//====================================================================================================
// コンストラクタ
Collider::Collider(const int width, const int height, const bool is_correct, const bool is_trigger)
	: width_(width)
	, height_(height)
	, is_correct_(is_correct)
	, is_trigger_(is_trigger)
{}

//====================================================================================================
// 当たり判定と当たった時の関数呼び出し
void Collider::hitActionGameObjects(GameObject& game_object1, const t2k::Vector3& before_position1, GameObject& game_object2, const t2k::Vector3& before_position2) {

	t2k::Vector3& p1 = game_object1.transform_.position_;
	t2k::Vector3 bp1 = before_position1;
	Collider* c1 = game_object1.collider_;
	float w1 = c1->width_ * game_object1.transform_.scale_.z;
	float h1 = c1->height_ * game_object1.transform_.scale_.z;
	t2k::Vector3& p2 = game_object2.transform_.position_;
	t2k::Vector3 bp2 = before_position2;
	Collider* c2 = game_object2.collider_;
	float w2 = c2->width_ * game_object2.transform_.scale_.z;
	float h2 = c2->height_ * game_object2.transform_.scale_.z;

	// 当たっている
	if (t2k::isIntersectRect(p1, w1, h1, p2, w2, h2)) {

		// 方向
		int d1 = 0;
		int d2 = 0;

		// どちらもトリガーではない
		if (!c1->is_trigger_ && !c2->is_trigger_) {
			// 座標補正フラグが立っているGameObjectを座標補正する
			// game_object1を座標補正する
			if (c1->is_correct_ && !c2->is_correct_) {
				d1 = t2k::isIntersectRectToCorrectPosition(p1, bp1, w1, h1, p2, w2, h2);	
				// もし上に補正して、重力の影響を受けている場合は速度を0にする
				if (game_object1.receive_gravity_) {
					if (d1 == 3 || (d1 == 4 && game_object1.receive_gravity_->velocity_.y < 0)) {
						game_object1.receive_gravity_->velocity_.y = 0;
					}
				}
			}
			// game_object2を座標補正する
			if (!c1->is_correct_ && c2->is_correct_) {
				d2 = t2k::isIntersectRectToCorrectPosition(p2, bp2, w2, h2, p1, w1, h1);
				// もし上に補正して、重力の影響を受けている場合は速度を0にする
				if (game_object2.receive_gravity_) {
					if (d2 == 3 || (d2 == 4 && game_object2.receive_gravity_->velocity_.y < 0)) {
						game_object2.receive_gravity_->velocity_.y = 0;
					}
				}
			}
		}

		// どちらかがトリガーの場合は座標補正しない
		// 座標補正フラグが両方trueか両方falseの場合は座標補正しない
		game_object1.hit(game_object2, d1);
		game_object2.hit(game_object1, d2);
	}
}
