#pragma once
#include <algorithm>
#include "../../library/t2klib.h"


class GameObject;

class ReceiveGravity {
private:
	//====================================================================================================
	// メンバ変数
	GameObject* game_object_;				// このクラスがアタッチされているGameObjectのポインタ
	float affect_gravity_rate_ = 1.0f;		// 重力の影響割合(2.0〜0.0)

public:
	//====================================================================================================
	// コンストラクタ（引数はthisポインタ）
	ReceiveGravity(GameObject* game_object);

	//====================================================================================================
	// デストラクタ
	~ReceiveGravity() {}

	//====================================================================================================
	// メンバ変数
	t2k::Vector3 velocity_;		// 速度

	//====================================================================================================
	// メンバ関数

	// 重力の影響を与える
	void affectedByGravity(const float gravity);

	// セッター
	inline void setAffectGravityRate(const float affect_gravity_rate) { affect_gravity_rate_ = std::clamp(affect_gravity_rate, 0.0f, 2.0f); }
};
