#pragma once
#include <string>


class Transform;

class String {
private:
	//====================================================================================================
	// メンバ変数
	std::string* str_ = nullptr;		// 文字列
	int rows_ = 1;						// 行数

public:
	//====================================================================================================
	// コンストラクタ
	String(const char* str = "NewString", ...);

	// コピーコンストラクタ
	String(const String& other);
	
	//====================================================================================================
	// デストラクタ
	~String();

	//====================================================================================================
	// メンバ関数

	// 文字列の設定
	void setString(const char* str, ...);

	// 描画
	void render(const Transform& transform, const int font_size, const int color, const int interval = 0);

	// 描画
	void render(const int x, const int y, const int font_size, const int color, const int interval = 0);

	// 文字列の長さを取得
	inline int length() {
		int length = 0;
		for (int i = 0; i < rows_; ++i) {
			length += str_[i].length();
		}
		return length;
	}
};
