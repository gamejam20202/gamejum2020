#include "DxLib.h"
#include "transform_.h"
#include "string_.h"


//====================================================================================================
// コンストラクタ
String::String(const char* str, ...) {
	char buff[255] = { 0 };
	va_list argptr;

	va_start(argptr, str);
	vsprintf(buff, str, argptr);
	va_end(argptr);

	std::string tmp = buff;

	for (int i = 0; i < tmp.length(); ++i) {
		if (tmp[i] == '\n') {
			++rows_;
		}
	}

	str_ = new std::string[rows_];
	int row = 0;

	for (int i = 0; i < tmp.length(); ++i) {
		if (tmp[i] == '\n') {
			++row;
			continue;
		}
		str_[row] += tmp[i];
	}
}

//====================================================================================================
// コピーコンストラクタ
String::String(const String& other)
	: rows_(other.rows_) {
	str_ = new std::string[rows_];
	for (int i = 0; i < rows_; ++i) {
		str_[i] = other.str_[i];
	}
}

//====================================================================================================
// デストラクタ
String::~String() {
	if (str_) {
		delete[] str_;
	}
}

//====================================================================================================
// 文字列の設定
void String::setString(const char* str, ...) {
	char buff[255] = { 0 };
	va_list argptr;

	va_start(argptr, str);
	vsprintf(buff, str, argptr);
	va_end(argptr);

	std::string tmp = buff;

	if (str_) {
		delete[] str_;
	}

	rows_ = 1;

	for (int i = 0; i < tmp.length(); ++i) {
		if (tmp[i] == '\n') {
			++rows_;
		}
	}

	str_ = new std::string[rows_];
	int row = 0;

	for (int i = 0; i < tmp.length(); ++i) {
		if (tmp[i] == '\n') {
			++row;
			continue;
		}
		str_[row] += tmp[i];
	}
}

//====================================================================================================
// 描画
void String::render(const Transform& transform, const int font_size, const int color, const int interval) {
	render(transform.position_.x, transform.position_.y, font_size, color, interval);
}

//====================================================================================================
// 描画
void String::render(const int x, const int y, const int font_size, const int color, const int interval) {
	SetFontSize(font_size);
	for (int i = 0; i < rows_; ++i) {
		DrawString(x, y + i * font_size + i * interval, str_[i].c_str(), color);
	}
}
