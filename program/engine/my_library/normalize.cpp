#include <algorithm>
#include "normalize.h"


//====================================================================================================
// コンストラクタ
Normalize::Normalize(const float incrase_per_frame)
	: incrase_per_frame_(incrase_per_frame)
{}

//====================================================================================================
// UnityのGetAxisのHorizontal, Verticalみたいな感じ
float Normalize::getValue(const bool negative, const bool positive) {
	const float MAX = 1;
	const float MIN = -1;

	if (negative && !positive) {
		value_ -= incrase_per_frame_;
	}

	if (positive && !negative) {
		value_ += incrase_per_frame_;
	}

	if ((negative && positive) || (!negative && !positive)) {
		if (value_ < 0) {
			if (incrase_per_frame_ < value_) {
				value_ = 0;
			}
			else {
				value_ += incrase_per_frame_;
			}
		}

		if (0 < value_) {
			if (value_ < incrase_per_frame_) {
				value_ = 0;
			}
			else {
				value_ -= incrase_per_frame_;
			}
		}
	}

	value_ = std::clamp(value_, MIN, MAX);

	return value_;
}
