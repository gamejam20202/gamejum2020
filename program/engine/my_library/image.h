#pragma once
#include <string>


class Transform;

class Image {
public:
	//====================================================================================================
	// 列挙定数
	enum AnimationType {
		ANIMTYPE_LOOP,			// ループ
		ANIMTYPE_ROUND_TRIP		// 往復
	};

	//====================================================================================================
	// 分割ロードクラス
	class DivInfo {
	private:
		//====================================================================================================
		// メンバ変数
		std::string file_name_;			// ファイル名
		int all_num_ = 0;				// 画像の総数
		int x_num_ = 0;					// 画像の横方向の数
		int y_num_ = 0;					// 画像の縦方向の数
		int width_ = 0;					// 画像1枚の幅
		int height_ = 0;				// 画像1枚の高さ
		int c_file_name_index_ = 0;		// 読み込み、書き込み用のファイル名の文字数

	public:
		//====================================================================================================
		// コンストラクタ
		DivInfo() {}

		//====================================================================================================
		// コンストラクタ
		DivInfo(const char* file_name, int all_num, int x_num, int y_num, int width, int height);

		//====================================================================================================
		// デストラクタ
		~DivInfo() {}

		//====================================================================================================
		// メンバ関数

		// ファイルに書き込み
		void fWrite(FILE* fp);

		// ファイルから読み込み
		void fRead(FILE* fp);

		// ゲッター
		inline const char* getFileName() const { return file_name_.c_str(); }
		inline int		   getAllNum()	 const { return all_num_; }
		inline int		   getXNum()	 const { return x_num_; }
		inline int		   getYNum()	 const { return y_num_; }
		inline int		   getWidth()	 const { return width_; }
		inline int		   getHeight()	 const { return height_; }
	};

private:
	//====================================================================================================
	// メンバ変数
	int* handle_ = nullptr;	// 画像ハンドル
	int all_num_;			// 画像の総数
	int width_;				// 幅
	int height_;			// 高さ

	int animation_ = 0;									// 現在のアニメーション
	int animation_incrase_ = 1;							// アニメーションの増加量
	AnimationType animation_type_ = ANIMTYPE_LOOP;		// アニメーションのタイプ

	float time_count_ = 0.0f;		// アニメーション用カウンタ
	float switch_time_ = 0.2f;		// アニメーション切り替え間隔

public:
	//====================================================================================================
	// コンストラクタ
	Image();

	// コンストラクタ
	Image(const char* file_name);

	// コンストラクタ
	Image(const char* file_name, const int all_num, const int x_num, const int y_num, const int width, const int height);
	
	// コンストラクタ
	Image(const DivInfo& div_info);

	// コピーコンストラクタ
	Image(const Image& other);

	//====================================================================================================
	// デストラクタ
	~Image();

	//====================================================================================================
	// メンバ関数

	// 画像のロード
	void loadImage(const char* file_name);

	// 画像の分割ロード
	void loadDivImage(const char* file_name, const int all_num, const int x_num, const int y_num, const int width, const int height);

	// 画像の分割ロード
	inline void loadDivImage(const DivInfo& div_info) { loadDivImage(div_info.getFileName(), div_info.getAllNum(), div_info.getXNum(), div_info.getYNum(), div_info.getWidth(), div_info.getHeight()); }

	// 更新処理 アニメーションのみ
	void update(const float delta_time);
	
	// 描画
	void render(const Transform& transform, const bool turn = false);

	// 描画
	void render(const int x, const int y, const double scale, const double angle, const bool turn = false);

	// セッター
	inline void setSwitchTime(const float switch_time) { switch_time_ = switch_time; }
	inline void setAnimationType(const AnimationType animation_type) { animation_type_ = animation_type; }

	// ゲッター
	inline int getWidth() { return width_; }
	inline int getHeight() { return height_; }
};
