#include "receive_gravity.h"
#include "../object/game_object/game_object.h"


//====================================================================================================
// コンストラクタ
ReceiveGravity::ReceiveGravity(GameObject* game_object)
	: game_object_(game_object)
{}

//====================================================================================================
// 重力の影響を与える
void ReceiveGravity::affectedByGravity(const float gravity) {
	game_object_->transform_.position_ += velocity_;
	velocity_.y += (gravity * affect_gravity_rate_);
}
