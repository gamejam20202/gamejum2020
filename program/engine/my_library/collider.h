#pragma once
#include "../../library/t2klib.h"


class GameObject;

class Collider {
private:
	//====================================================================================================
	// メンバ変数
	int width_;				// 幅
	int height_;			// 高さ
	bool is_correct_;		// 座標補正フラグ
	bool is_trigger_;		// トリガーフラグ

public:
	//====================================================================================================
	// コンストラクタ
	Collider(const int width, const int height, const bool is_correct = false, const bool is_trigger = false);

	//====================================================================================================
	// デストラクタ
	~Collider() {}

	//====================================================================================================
	// メンバ関数

	// セッター
	inline void setWidth(const int width) { width_ = width; }
	inline void setHeight(const int height) { height_ = height; }
	inline void setIsCottect(const bool is_correct) { is_correct_ = is_correct; }
	inline void setIsTrigger(const bool is_trigger) { is_trigger_ = is_trigger; }

	//====================================================================================================
	// 静的メンバ関数

	// 当たり判定と当たった時の関数呼び出し
	static void hitActionGameObjects(GameObject& game_object1, const t2k::Vector3& before_position1, GameObject& game_object2, const t2k::Vector3& before_position2);
};
