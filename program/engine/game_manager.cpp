#include "game_manager.h"
#include "scene/scene.h"
#include "../game/scene/title.h"


// 静的メンバ変数の初期化
GameManager* GameManager::instance_ = nullptr;

//====================================================================================================
// 初期化シーケンス
bool GameManager::seqInit(const float delta_time) {

	//----------------//
	// 初期シーン生成 //
	//----------------//
	new Title();
	
	now_ = next_;

	seq_.change(&GameManager::seqIdle);

	return true;
}

//====================================================================================================
// アイドルシーケンス
bool GameManager::seqIdle(const float delta_time) {

	now_->update(delta_time);

	return true;
}

//====================================================================================================
// 更新
void GameManager::update(const float delta_time) {

	if (now_ != next_) {
		if (now_) {
			delete now_;
		}
		now_ = next_;
	}

	seq_.update(delta_time);

}

//====================================================================================================
// 描画
void GameManager::render() {
	now_->render();
	debug_.render();
}

//====================================================================================================
// 乱数シード値のロード
void GameManager::loadSeed() {
	FILE* fp;
	fopen_s(&fp, "data/random_seed.dat", "rb");
	if (fp == nullptr) return;
	fread(&seed_, sizeof(seed_), 1, fp);
	fclose(fp);
}

//====================================================================================================
// 乱数シード値のセーブ
void GameManager::saveSeed() {
	FILE* fp;
	fopen_s(&fp, "data/random_seed.dat", "wb");
	if (fp == nullptr) return;
	fwrite(&seed_, sizeof(seed_), 1, fp);
	fclose(fp);
}

//====================================================================================================
// シングルトンなインスタンスの取得
GameManager* GameManager::getInstance() {
	if (!instance_) {
		instance_ = new GameManager();
	}
	return instance_;
}
