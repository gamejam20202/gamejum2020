#pragma once
#include "../library/t2klib.h"
#include "my_library/debug.h"


class Scene;

class GameManager {
private:
	//====================================================================================================
	// コンストラクタ
	GameManager() {}

	//====================================================================================================
	// デストラクタ
	~GameManager() {}

	//====================================================================================================
	// メンバ変数
	t2k::Sequence<GameManager*> seq_ = t2k::Sequence<GameManager*>(this, &GameManager::seqInit);		// シーケンス処理
	Scene* now_ = nullptr;		// 現在のシーン
	Scene* next_ = nullptr;		// 次のシーン
	unsigned int seed_ = 0;		// 乱数シード値

	//====================================================================================================
	// 静的メンバ変数
	static GameManager* instance_;		// シングルトンなインスタンス

	//====================================================================================================
	// メンバ関数

	// 初期化シーケンス
	bool seqInit(const float delta_time);
	
	// アイドルシーケンス
	bool seqIdle(const float delta_time);

public:
	//====================================================================================================
	// メンバ変数
	Debug debug_;		// デバッグ用

	//====================================================================================================
	// メンバ関数

	// 更新
	void update(const float delta_time);
	
	// 描画
	void render();

	// 乱数シード値のロード
	void loadSeed();

	// 乱数シード値のセーブ
	void saveSeed();

	// 乱数シードの設定
	inline void setRandomSeed() { srand(seed_); }

	// セッター
	inline void setNextScene(Scene* next) { next_ = next; }
	inline void setSeed(const unsigned int seed) { seed_ = seed; }

	// ゲッター
	inline Scene* getNowScene() { return now_; }

	//====================================================================================================
	// 静的メンバ関数

	// シングルトンなインスタンスの取得
	static GameManager* getInstance();
};
